//
//  ViewExtension.swift
//  Omna
//
//  Created by Professor on 19/05/2017.
//  Copyright © 2017 D-Link. All rights reserved.
//

import UIKit

extension UIView {
    public func addAndCenter(subView: UIView) {
        subView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subView)
        subView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        subView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        //        addConstraints([NSLayoutConstraint(item: subView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)])
    }

    public func insertAndCenter(subView: UIView, at: Int) {
        subView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(subView, at: at)
        subView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        subView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        //        addConstraints([NSLayoutConstraint(item: subView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)])
    }

    public func addAndFill(subView: UIView, useSafeArea: Bool = true) {
        subView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subView)
        if useSafeArea, #available(iOS 11.0, *) {
            subView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor).isActive = true
            subView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor).isActive = true
            subView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
            subView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            subView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
            subView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
            subView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            subView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
        //        addConstraints([NSLayoutConstraint(item: subView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)])
    }

    public func insertAndFill(subView: UIView, at: Int, useSafeArea: Bool = true) {
        subView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(subView, at: at)
        if useSafeArea, #available(iOS 11.0, *) {
            subView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor).isActive = true
            subView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor).isActive = true
            subView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
            subView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            subView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
            subView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
            subView.topAnchor.constraint(equalTo: topAnchor).isActive = true
            subView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        }
        //        addConstraints([NSLayoutConstraint(item: subView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
        //                        NSLayoutConstraint(item: subView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)])
    }

    public func addLeadingBorder(withColor color: UIColor, width: CGFloat) {
        let layer = CALayer()
        layer.backgroundColor = color.cgColor
        layer.frame = CGRect(x: 0, y: 0, width: width, height: bounds.height)
        self.layer.addSublayer(layer)
    }

    public func addTrailingBorder(withColor color: UIColor, width: CGFloat) {
        let layer = CALayer()
        layer.backgroundColor = color.cgColor
        layer.frame = CGRect(x: bounds.width - width, y: 0, width: width, height: bounds.height)
        self.layer.addSublayer(layer)
    }

    public func addTopBorder(withColor color: UIColor, width: CGFloat) {
        let layer = CALayer()
        layer.backgroundColor = color.cgColor
        layer.frame = CGRect(x: 0, y: 0, width: bounds.width, height: width)
        self.layer.addSublayer(layer)
    }

    public func addBottomBorder(withColor color: UIColor, width: CGFloat) {
        let layer = CALayer()
        layer.backgroundColor = color.cgColor
        layer.frame = CGRect(x: 0, y: bounds.height - width, width: bounds.width, height: width)
        self.layer.addSublayer(layer)
    }
}
