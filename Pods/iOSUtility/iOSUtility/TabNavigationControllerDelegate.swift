//
//  TabNavigationControllerDelegate.swift
//  BonnieDraw
//
//  Created by Professor on 08/11/2017.
//  Copyright © 2017 Professor. All rights reserved.
//

import UIKit

public class TabNavigationControllerDelegate: NSObject, UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .push && navigationController.viewControllers.count == 1 {
            return TabNavigationControllerAnimator()
        }
        return nil
    }
}
