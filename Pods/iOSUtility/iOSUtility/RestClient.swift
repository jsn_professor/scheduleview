//
//  RestClient.swift
//  iOSTemplate
//
//  Created by Professor on 1/25/16.
//  Copyright © 2016 Professor. All rights reserved.
//

import Foundation

open class RestClient: NSObject, URLSessionTaskDelegate {
    public weak var restClientDelegate: RestClientDelegate?
    var session: URLSession?
    var task: URLSessionDataTask?

    public init(configuration: URLSessionConfiguration = .default) {
        super.init()
        session = URLSession(configuration: configuration, delegate: self, delegateQueue: .main)
    }

    public func getResponse(httpMethod: String = "GET",
                            url: String,
                            queries: [URLQueryItem]? = nil,
                            headers: [String: String]? = nil,
                            body: [String: Any]? = nil,
                            completionHandler: @escaping (Bool, [String: Any]?, [[String: Any]]?) -> Void) {
        var components = URLComponents(string: url)
        components?.queryItems = queries
        guard let url = components?.url else {
            Logger.d("\(#function) Error unwrapping url")
            DispatchQueue.main.async {
                completionHandler(false, nil, nil)
            }
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.allHTTPHeaderFields = headers
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if let body = body {
            if request.httpMethod == "GET" {
                request.httpMethod = "POST"
            }
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: [])
        }
        getResponse(request: request, completionHandler: completionHandler)
    }

    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge,
                           completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        switch challenge.protectionSpace.authenticationMethod {
        case NSURLAuthenticationMethodServerTrust:
            if let trust = challenge.protectionSpace.serverTrust {
                completionHandler(.useCredential, URLCredential(trust: trust))
            }
        default:
            challenge.sender?.performDefaultHandling?(for: challenge)
        }
    }

    public func cancel() {
        task?.cancel()
    }

    private func getResponse(request: URLRequest, completionHandler: @escaping (Bool, [String: Any]?, [[String: Any]]?) -> Void) {
        cancel()
        self.task = session?.dataTask(with: request) {
            data, response, error in
            if let error = error as NSError? {
                Logger.d("\(#function) \(request.url?.absoluteString ?? ""), \(error.localizedDescription)")
                if error.code != NSURLErrorCancelled {
                    DispatchQueue.main.async {
                        completionHandler(false, nil, nil)
                    }
                }
            } else {
                guard let response = response as? HTTPURLResponse else {
                    Logger.d("\(#function) \(request.url?.absoluteString ?? "")")
                    DispatchQueue.main.async {
                        completionHandler(false, nil, nil)
                    }
                    return
                }
                let resultObject: [String: Any]?
                let resultArray: [[String: Any]]?
                if let data = data {
                    let result = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    resultObject = result as? [String: Any]
                    resultArray = result as? [[String: Any]]
                } else {
                    resultObject = nil
                    resultArray = nil
                }
                guard !(self.restClientDelegate?.restClientDidFinish(request: request,
                        statusCode: response.statusCode,
                        resultObject: resultObject,
                        resultArray: resultArray,
                        completionHandler: completionHandler) ?? false) else {
                    return
                }
                switch response.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        completionHandler(true, resultObject, resultArray)
                    }
                default:
                    DispatchQueue.main.async {
                        completionHandler(false, resultObject, resultArray)
                    }
                }
            }
        }
        DispatchQueue.global().async {
            self.task?.resume()
        }
    }
}

public protocol RestClientDelegate: class {
    func restClientDidFinish(request: URLRequest,
                             statusCode: Int,
                             resultObject: [String: Any]?,
                             resultArray: [[String: Any]]?,
                             completionHandler: @escaping (Bool, [String: Any]?, [[String: Any]]?) -> Void) -> Bool
}
