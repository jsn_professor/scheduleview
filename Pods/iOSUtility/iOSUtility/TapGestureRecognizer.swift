//
//  TapGestureRecognizer.swift
//  DlinkBusinessApp
//
//  Created by Professor on 10/04/2018.
//  Copyright © 2018 Professor. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

public class TapGestureRecognizer: UITapGestureRecognizer {
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        view?.alpha = 0.6
        super.touchesBegan(touches, with: event)
    }

    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        UIView.animate(withDuration: 0.3) {
            self.view?.alpha = 1
        }
        super.touchesEnded(touches, with: event)
    }

    public override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        UIView.animate(withDuration: 0.3) {
            self.view?.alpha = 1
        }
        super.touchesCancelled(touches, with: event)
    }
}
