//
//  WindowExtension.swift
//  iOSTemplate
//
//  Created by Professor on 08/03/2018.
//  Copyright © 2018 Agrowood. All rights reserved.
//

import UIKit

extension UIWindow {
    public var topViewController: UIViewController? {
        var controller = rootViewController
        while let presentedController = controller?.presentedViewController {
            controller = presentedController
        }
        if let navigationController = controller as? UINavigationController {
            controller = navigationController.topViewController
        }
        return controller
    }
}
