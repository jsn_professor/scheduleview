//
//  ScheduleControlView.swift
//  iOSTemplate
//
//  Created by Professor on 29/10/2017.
//  Copyright © 2017 Agrowood. All rights reserved.
//

import UIKit

class ScheduleControlView: UIView {
    private let tapGesture = UITapGestureRecognizer()
    var minutesPerTick: Int
    var alwaysShowDragLabel: Bool {
        didSet {
            if !alwaysShowDragLabel {
                removeLabels()
            }
        }
    }
    var padding: CGFloat {
        didSet {
            guard oldValue != padding else {
                return
            }
            setNeedsLayout()
        }
    }
    var itemSize = CGSize.zero {
        didSet {
            guard oldValue != itemSize else {
                return
            }
            setNeedsLayout()
        }
    }
    var gridFrame = CGRect.zero {
        didSet {
            guard oldValue != gridFrame else {
                return
            }
            setNeedsLayout()
        }
    }
    var dragLength: CGFloat = 0 {
        didSet {
            guard oldValue != dragLength else {
                return
            }
            setNeedsDisplay()
        }
    }
    var dragColor: UIColor? {
        didSet {
            guard oldValue != dragColor else {
                return
            }
            setNeedsDisplay()
        }
    }
    var topButtonImage: UIImage? {
        didSet {
            dragButtons?.top.image = topButtonImage
        }
    }
    var editButtonImage: UIImage? {
        didSet {
            dragButtons?.edit.setImage(editButtonImage, for: .normal)
        }
    }
    var bottomButtonImage: UIImage? {
        didSet {
            dragButtons?.bottom.image = bottomButtonImage
        }
    }
    var topButtonContentMode: UIView.ContentMode = .center {
        didSet {
            dragButtons?.top.contentMode = topButtonContentMode
        }
    }
    var bottomButtonContentMode: UIView.ContentMode = .center {
        didSet {
            dragButtons?.bottom.contentMode = bottomButtonContentMode
        }
    }
    var models: (previous: ScheduleModel?, current: ScheduleModel?, next: ScheduleModel?) {
        didSet {
            if let model = models.current {
                let rect = rectInGrid(model: model)
                if dragButtons == nil {
                    let topButton = UIImageView(image: topButtonImage)
                    topButton.bounds = CGRect(origin: .zero, size: CGSize(width: itemSize.width + (topButtonImage?.size.width ?? itemSize.width),
                            height: (topButtonImage?.size.height ?? itemSize.height) + 16))
                    topButton.contentMode = topButtonContentMode
                    topButton.center = CGPoint(x: rect.midX, y: rect.minY)
                    addSubview(topButton)
                    let editButton = UIButton()
                    editButton.setImage(editButtonImage, for: .normal)
                    editButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
                    editButton.sizeToFit()
                    editButton.addTarget(self, action: #selector(edit), for: .touchUpInside)
                    editButton.center = CGPoint(x: rect.midX, y: rect.midY)
                    addSubview(editButton)
                    let bottomButton = UIImageView(image: bottomButtonImage)
                    bottomButton.bounds = CGRect(origin: .zero, size: CGSize(width: itemSize.width + (bottomButtonImage?.size.width ?? itemSize.width),
                            height: (bottomButtonImage?.size.height ?? itemSize.height) + 16))
                    bottomButton.contentMode = bottomButtonContentMode
                    bottomButton.center = CGPoint(x: rect.midX, y: rect.maxY)
                    addSubview(bottomButton)
                    dragButtons = (topButton, editButton, bottomButton)
                }
                if let dragLabel = dragLabels.top {
                    dragLabel.center.y = rect.minY
                    dragLabel.text = "\(String(format: "%02d:%02d", model.timeInterval.start.hour, model.timeInterval.start.minute))"
                } else if alwaysShowDragLabel {
                    let dragLabel = createDragLabel(isTopButton: true, centerY: rect.minY, model: model)
                    addSubview(dragLabel)
                    dragLabels.top = dragLabel
                }
                if let dragLabel = dragLabels.bottom {
                    dragLabel.center.y = rect.maxY
                    dragLabel.text = "\(String(format: "%02d:%02d", model.timeInterval.end.hour, model.timeInterval.end.minute))"
                } else if alwaysShowDragLabel {
                    let dragLabel = createDragLabel(isTopButton: false, centerY: rect.maxY, model: model)
                    addSubview(dragLabel)
                    dragLabels.bottom = dragLabel
                }
                tapGesture.isEnabled = true
            } else {
                removeButtons()
                removeLabels()
            }
            setNeedsDisplay()
        }
    }
    private var dragMinY: CGFloat = 0
    private var dragMaxY: CGFloat = 0
    private var dragButtons: (top: UIImageView, edit: UIButton, bottom: UIImageView)?
    private var dragLabels: (top: UILabel?, bottom: UILabel?)
    private var previewRect: CGRect? {
        didSet {
            if let previewRect = previewRect {
                dragButtons?.top.center.y = previewRect.minY
                dragButtons?.edit.center.y = previewRect.midY
                dragButtons?.bottom.center.y = previewRect.maxY
            }
            setNeedsDisplay()
        }
    }
    private var isTopButton: Bool?
    weak var delegate: ScheduleControlViewDelegate?

    init(frame: CGRect,
         alwaysShowDragLabel: Bool,
         minutesPerTick: Int,
         padding: CGFloat,
         dragColor: UIColor?) {
        self.alwaysShowDragLabel = alwaysShowDragLabel
        self.minutesPerTick = minutesPerTick
        self.padding = padding
        self.dragColor = dragColor
        super.init(frame: frame)
        tapGesture.isEnabled = false
        tapGesture.addTarget(self, action: #selector(tap))
        addGestureRecognizer(tapGesture)
        translatesAutoresizingMaskIntoConstraints = false
        contentMode = .redraw
        backgroundColor = .clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        if let model = models.current {
            let rect = rectInGrid(model: model)
            dragButtons?.top.center = CGPoint(x: rect.midX, y: rect.minY)
            dragButtons?.edit.center = CGPoint(x: rect.midX, y: rect.midY)
            dragButtons?.bottom.center = CGPoint(x: rect.midX, y: rect.maxY)
            dragLabels.top?.frame = CGRect(
                    origin: CGPoint(
                            x: padding,
                            y: rect.minY - ScheduleContentView.LABEL_HEIGHT / 2),
                    size: CGSize(
                            width: itemSize.width,
                            height: ScheduleContentView.LABEL_HEIGHT))
            dragLabels.bottom?.frame = CGRect(
                    origin: CGPoint(
                            x: padding,
                            y: rect.maxY - ScheduleContentView.LABEL_HEIGHT / 2),
                    size: CGSize(
                            width: itemSize.width,
                            height: ScheduleContentView.LABEL_HEIGHT))
        }
        super.layoutSubviews()
    }

    override func draw(_ rect: CGRect) {
        guard let model = models.current else {
            return
        }
        let rect = rectInGrid(model: model)
        if let label = dragLabels.top {
            let path = UIBezierPath()
            path.move(to: CGPoint(x: label.frame.maxX, y: label.frame.midY))
            path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
            if let color = dragColor {
                color.setStroke()
            } else {
                model.selectedColor.setStroke()
            }
            path.stroke()
        }
        if let label = dragLabels.bottom {
            let path = UIBezierPath()
            path.move(to: CGPoint(x: label.frame.maxX, y: label.frame.midY))
            path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
            if let color = dragColor {
                color.setStroke()
            } else {
                model.selectedColor.setStroke()
            }
            path.stroke()
        }
        if let previewRect = previewRect {
            model.selectedColor.withAlphaComponent(0.5).setFill()
            UIBezierPath(rect: rect).fill()
            model.selectedColor.setFill()
            UIBezierPath(rect: previewRect).fill()
        } else {
            model.selectedColor.setFill()
            UIBezierPath(rect: rect).fill()
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let dragButtons = dragButtons,
              let model = models.current,
              let first = touches.first else {
            return
        }
        let location = first.location(in: self)
        if dragButtons.top.frame.contains(location) {
            tapGesture.isEnabled = false
            isTopButton = true
            let rect = rectInGrid(model: model)
            if let previous = models.previous {
                dragMinY = max(gridFrame.minY, rectInGrid(model: previous).maxY)
            } else {
                dragMinY = gridFrame.minY
            }
            dragMaxY = rect.maxY - dragLength
            previewRect = rect
            if dragLabels.top == nil {
                let dragLabel = createDragLabel(isTopButton: true, centerY: rect.minY, model: model)
                addSubview(dragLabel)
                dragLabels.top = dragLabel
            }
        } else if dragButtons.bottom.frame.contains(location) {
            tapGesture.isEnabled = false
            isTopButton = false
            let rect = rectInGrid(model: model)
            dragMinY = rect.minY + dragLength
            if let next = models.next {
                dragMaxY = min(gridFrame.maxY, rectInGrid(model: next).minY)
            } else {
                dragMaxY = gridFrame.maxY
            }
            previewRect = rect
            if dragLabels.bottom == nil {
                let dragLabel = createDragLabel(isTopButton: false, centerY: rect.maxY, model: model)
                addSubview(dragLabel)
                dragLabels.bottom = dragLabel
            }
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let isTopButton = isTopButton,
              var model = models.current,
              let first = touches.first else {
            return
        }
        let location = first.location(in: self)
        let previousLocation = first.previousLocation(in: self)
        if let contentView = superview as? ScheduleContentView,
           let scrollView = contentView.superview as? UIScrollView,
           scrollView.isScrollEnabled && scrollView.contentSize.height > scrollView.bounds.height {
            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y + location.y - previousLocation.y)
        }
        let rect = rectInGrid(model: model)
        let newRect: CGRect
        var y: CGFloat
        if isTopButton {
            y = rect.minY
            if Float(location.y) > Float(y) {
                while Float(y) < Float(location.y) {
                    if Float(y + dragLength) > Float(min(location.y, dragMaxY)) {
                        break
                    }
                    y += dragLength
                }
            } else {
                while Float(y) > Float(location.y) {
                    if Float(y - dragLength) < Float(dragMinY) {
                        y = dragMinY
                        break
                    }
                    y -= dragLength
                }
            }
            newRect = CGRect(
                    x: rect.minX,
                    y: y,
                    width: rect.width,
                    height: rect.maxY - y)
            previewRect = CGRect(
                    x: rect.minX,
                    y: max(min(rect.maxY, location.y), dragMinY),
                    width: rect.width,
                    height: max(rect.maxY - max(location.y, dragMinY), 0))
        } else {
            y = rect.maxY
            if Float(location.y) > Float(y) {
                while Float(y) < Float(location.y) {
                    if Float(y + dragLength) > Float(dragMaxY) {
                        y = dragMaxY
                        break
                    }
                    y += dragLength
                }
            } else {
                while Float(y) > Float(location.y) {
                    if Float(y - dragLength) < Float(max(location.y, dragMinY)) {
                        break
                    }
                    y -= dragLength
                }
            }
            newRect = CGRect(
                    x: rect.minX,
                    y: rect.minY,
                    width: rect.width,
                    height: y - rect.minY)
            previewRect = CGRect(
                    x: rect.minX,
                    y: rect.minY,
                    width: rect.width,
                    height: max(min(location.y, dragMaxY) - rect.minY, 0))
        }
        if rect != newRect {
            let minutePerLength = CGFloat(minutesPerTick) / itemSize.height
            if isTopButton {
                model.timeInterval.start.valueInMinutes = Int(round((newRect.minY - gridFrame.minY) * minutePerLength))
            } else {
                model.timeInterval.end.valueInMinutes = Int(round((newRect.maxY - gridFrame.minY) * minutePerLength))
            }
            if isTopButton {
                dragLabels.top?.center.y = newRect.minY
            } else {
                dragLabels.bottom?.center.y = newRect.maxY
            }
            models.current = model
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesFinished()
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesFinished()
    }

    private func touchesFinished() {
        if !alwaysShowDragLabel {
            removeLabels()
        }
        if isTopButton != nil {
            tapGesture.isEnabled = true
            isTopButton = nil
            previewRect = nil
            if let model = models.current {
                let rect = rectInGrid(model: model)
                dragButtons?.top.center.y = rect.minY
                dragButtons?.edit.center.y = rect.midY
                dragButtons?.bottom.center.y = rect.maxY
            }
            if let contentView = superview as? ScheduleContentView,
               let scrollView = contentView.superview as? UIScrollView,
               scrollView.isScrollEnabled && scrollView.contentSize.height > scrollView.bounds.height {
                if scrollView.contentOffset.y < 0 {
                    if #available(iOS 11.0, *) {
                        scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.safeAreaInsets.top), animated: true)
                    } else {
                        scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentInset.top), animated: true)
                    }
                } else if scrollView.contentOffset.y + scrollView.bounds.height > scrollView.contentSize.height {
                    if #available(iOS 11.0, *) {
                        scrollView.setContentOffset(CGPoint(x: 0,
                                y: scrollView.safeAreaInsets.bottom + scrollView.contentSize.height - scrollView.bounds.height),
                                animated: true)
                    } else {
                        scrollView.setContentOffset(CGPoint(x: 0,
                                y: scrollView.contentInset.bottom + scrollView.contentSize.height - scrollView.bounds.height),
                                animated: true)
                    }
                }
            }
        }
    }

    private func createDragLabel(isTopButton: Bool, centerY: CGFloat, model: ScheduleModel) -> UILabel {
        let dragLabel = UILabel(
                frame: CGRect(
                        origin: CGPoint(
                                x: padding,
                                y: centerY - ScheduleContentView.LABEL_HEIGHT / 2),
                        size: CGSize(
                                width: itemSize.width,
                                height: ScheduleContentView.LABEL_HEIGHT)))
        dragLabel.layer.cornerRadius = ScheduleContentView.LABEL_HEIGHT / 2
        dragLabel.clipsToBounds = true
        dragLabel.textColor = .white
        dragLabel.font = UIFont.systemFont(ofSize: 12)
        dragLabel.textAlignment = .center
        dragLabel.adjustsFontSizeToFitWidth = true
        dragLabel.backgroundColor = dragColor ?? model.selectedColor
        let model = isTopButton ? model.timeInterval.start : model.timeInterval.end
        dragLabel.text = "\(String(format: "%02d:%02d", model.hour, model.minute))"
        return dragLabel
    }

    private func removeButtons() {
        dragButtons?.top.removeFromSuperview()
        dragButtons?.edit.removeFromSuperview()
        dragButtons?.bottom.removeFromSuperview()
        dragButtons = nil
    }

    private func removeLabels() {
        dragLabels.top?.removeFromSuperview()
        dragLabels.bottom?.removeFromSuperview()
        dragLabels = (nil, nil)
    }

    private func rectInGrid(model: ScheduleModel) -> CGRect {
        return CGRect(
                origin: CGPoint(
                        x: gridFrame.minX + itemSize.width * CGFloat(model.dayOfWeek.rawValue),
                        y: gridFrame.minY + (CGFloat(model.timeInterval.start.hour) * 60 + CGFloat(model.timeInterval.start.minute)) / CGFloat(minutesPerTick) * itemSize.height),
                size: CGSize(
                        width: itemSize.width,
                        height: (CGFloat(model.timeInterval.end.hour - model.timeInterval.start.hour) * 60 + CGFloat(model.timeInterval.end.minute - model.timeInterval.start.minute)) / CGFloat(minutesPerTick) * itemSize.height))
    }

    @objc private func edit(_ sender: UIButton) {
        guard let model = models.current else {
            return
        }
        delegate?.scheduleControlDidTapEdit(model: model)
    }

    @objc private func tap(gestureRecognizer: UITapGestureRecognizer) {
        guard isTopButton == nil,
              let model = models.current,
              !rectInGrid(model: model).contains(gestureRecognizer.location(in: self)) else {
            return
        }
        tapGesture.isEnabled = false
        models = (nil, nil, nil)
        delegate?.scheduleControlDidEndEditing(model: model)
    }

    func endEditing() -> ScheduleModel? {
        guard let model = models.current else {
            return nil
        }
        tapGesture.isEnabled = false
        models = (nil, nil, nil)
        return model
    }
}

protocol ScheduleControlViewDelegate: AnyObject {
    func scheduleControlDidTapEdit(model: ScheduleModel)

    func scheduleControlDidEndEditing(model: ScheduleModel)

    func scheduleControlDidRemove(model: ScheduleModel)
}
