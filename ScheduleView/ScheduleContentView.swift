//
//  ScheduleContentView.swift
//  iOSTemplate
//
//  Created by Professor on 26/10/2017.
//  Copyright © 2017 Agrowood. All rights reserved.
//

import UIKit

class ScheduleContentView: UIView, ScheduleControlViewDelegate, UIGestureRecognizerDelegate {
//    override class var layerClass: AnyClass {
//        return ScheduleLayer.self
//    }
    public static let LABEL_HEIGHT: CGFloat = 21
    private let tapGesture = UITapGestureRecognizer()
    private let longPressGesture = UILongPressGestureRecognizer()
    private let pinchGesture = UIPinchGestureRecognizer()
    private var heightConstraint: NSLayoutConstraint!
    private var dayLabels = [UILabel]()
    private var timeLabels = [UILabel]()
    private var itemSize = CGSize.zero
    private var gridFrame = CGRect.zero
    private var dragLength: CGFloat = 0
    private var gridRadioBounds: (minimum: CGFloat, maximum: CGFloat)
    private var pinchInitialState: (itemHeight: CGFloat, touchPositionDifference: CGFloat) = (0, 0)
    private var tickCount: Int {
        return 1440 / minutesPerTick
    }
    var showWeekdayLabels: Bool {
        didSet {
            guard oldValue != showWeekdayLabels else {
                return
            }
            if showWeekdayLabels {
                let formatter = DateFormatter()
                for index in 0..<formatter.shortWeekdaySymbols.count {
                    let label = UILabel(frame: CGRect(x: gridFrame.minX + CGFloat(index) * itemSize.width,
                            y: padding,
                            width: itemSize.width,
                            height: ScheduleContentView.LABEL_HEIGHT))
                    label.text = formatter.shortWeekdaySymbols[index]
                    label.textAlignment = .center
                    label.font = UIFont.systemFont(ofSize: 15)
                    label.adjustsFontSizeToFitWidth = true
                    label.textColor = labelColor
                    addSubview(label)
                    dayLabels.append(label)
                }
            } else {
                dayLabels.forEach({ $0.removeFromSuperview() })
                dayLabels = []
            }
            setNeedsDisplay()
        }
    }
    var dragOverflow: Bool
    var labelColor: UIColor {
        didSet {
            guard oldValue != labelColor else {
                return
            }
            for label in dayLabels {
                label.textColor = labelColor
            }
            for label in timeLabels {
                label.textColor = labelColor
            }
        }
    }
    var padding: CGFloat {
        didSet {
            guard oldValue != padding else {
                return
            }
            setNeedsLayout()
        }
    }
    var gridRadio: CGFloat {
        didSet {
            guard oldValue != gridRadio else {
                return
            }
            setNeedsLayout()
        }
    }
    var zoomScale: (minimum: CGFloat, maximum: CGFloat) {
        didSet {
            guard oldValue != zoomScale else {
                return
            }
            gridRadioBounds = (gridRadio * zoomScale.minimum, gridRadio * zoomScale.maximum)
        }
    }
    var createMinutes: Int
    var minutesPerTick: Int
    var minutesPerStep: Int
    var gridLayer: ScheduleGridLayer!
    var controlView: ScheduleControlView!
    var barColor: UIColor
    var barSelectedColor: UIColor
    var models = [ScheduleModel]() {
        didSet {
            var merged = [ScheduleModel]()
            for var model in models.sorted() {
                guard model.timeInterval.start != model.timeInterval.end else {
                    continue
                }
                guard var previous = merged.last,
                      previous.dayOfWeek == model.dayOfWeek,
                      previous.timeInterval.end >= model.timeInterval.start else {
                    merged.append(model)
                    continue
                }
                if previous.isMergeable(model: model) {
                    previous.timeInterval.end = max(previous.timeInterval.end, model.timeInterval.end)
                    merged[merged.count - 1] = previous
                } else if previous.timeInterval.end < model.timeInterval.end {
                    model.timeInterval.start = previous.timeInterval.end
                    merged.append(model)
                }
            }
            models = merged
            setNeedsDisplay()
        }
    }
    weak var delegate: ScheduleContentViewDelegate?

    init(frame: CGRect,
         alwaysShowDragLabel: Bool,
         showWeekdayLabels: Bool,
         dragOverflow: Bool,
         verticalDashLines: Bool,
         horizontalDashLines: Bool,
         createMinutes: Int,
         minutesPerTick: Int,
         minutesPerStep: Int,
         padding: CGFloat,
         gridRadio: CGFloat,
         zoomScale: (minimum: CGFloat, maximum: CGFloat),
         dragColor: UIColor?,
         barColor: UIColor,
         barSelectedColor: UIColor,
         labelColor: UIColor,
         gridColor: UIColor) {
        self.showWeekdayLabels = showWeekdayLabels
        self.dragOverflow = dragOverflow
        self.createMinutes = createMinutes
        self.minutesPerTick = minutesPerTick
        self.minutesPerStep = minutesPerStep
        self.padding = padding
        self.gridRadio = gridRadio
        self.zoomScale = zoomScale
        self.gridRadioBounds = (gridRadio * zoomScale.minimum, gridRadio * zoomScale.maximum)
        self.barColor = barColor
        self.barSelectedColor = barSelectedColor
        self.labelColor = labelColor
        super.init(frame: frame)
        tapGesture.addTarget(self, action: #selector(tap))
        addGestureRecognizer(tapGesture)
        longPressGesture.addTarget(self, action: #selector(longPress))
        addGestureRecognizer(longPressGesture)
        pinchGesture.addTarget(self, action: #selector(pinch))
        pinchGesture.delegate = self
        addGestureRecognizer(pinchGesture)
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        contentMode = .redraw
        heightConstraint = heightAnchor.constraint(equalToConstant: frame.height)
        heightConstraint.isActive = true
        let gridLayer = ScheduleGridLayer(
                verticalDashLines: verticalDashLines,
                horizontalDashLines: horizontalDashLines,
                gridColor: gridColor,
                minutesPerTick: minutesPerTick)
        layer.addSublayer(gridLayer)
        self.gridLayer = gridLayer
        let formatter = DateFormatter()
        if showWeekdayLabels {
            for index in 0..<formatter.shortWeekdaySymbols.count {
                let label = UILabel()
                label.text = formatter.shortWeekdaySymbols[index]
                label.textAlignment = .center
                label.font = UIFont.systemFont(ofSize: 15)
                label.adjustsFontSizeToFitWidth = true
                label.textColor = labelColor
                addSubview(label)
                dayLabels.append(label)
            }
        }
        formatter.dateFormat = "HH:mm"
        var date = Calendar.current.date(from: DateComponents(hour: 0, minute: 0))!
        for index in 0...tickCount {
            let label = UILabel()
            label.text = formatter.string(from: date)
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 12)
            label.adjustsFontSizeToFitWidth = true
            label.textColor = labelColor
            addSubview(label)
            timeLabels.append(label)
            if index < tickCount {
                date.addTimeInterval(TimeInterval(minutesPerTick * 60))
            } else {
                label.text = "24:00"
            }
        }
        let controlView = ScheduleControlView(
                frame: frame,
                alwaysShowDragLabel: alwaysShowDragLabel,
                minutesPerTick: minutesPerTick,
                padding: padding,
                dragColor: dragColor)
        controlView.delegate = self
        addSubview(controlView)
        addConstraints([controlView.leadingAnchor.constraint(equalTo: leadingAnchor),
                        controlView.trailingAnchor.constraint(equalTo: trailingAnchor),
                        controlView.topAnchor.constraint(equalTo: topAnchor),
                        controlView.bottomAnchor.constraint(equalTo: bottomAnchor)])
        self.controlView = controlView
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        if let scheduleView = superview as? UIScrollView {
            var horizontalInset: CGFloat = 0
            if #available(iOS 11.0, *) {
                horizontalInset = scheduleView.safeAreaInsets.left + scheduleView.safeAreaInsets.right
            }
            let width = (bounds.width - 8 - horizontalInset - padding * 2) / 8
            let verticalPadding = (showWeekdayLabels ? ScheduleContentView.LABEL_HEIGHT + 8 : 0) + padding * 2
            if gridRadio <= 0 {
                var verticalInset: CGFloat = 0
                if #available(iOS 11.0, *) {
                    verticalInset = scheduleView.safeAreaInsets.top + scheduleView.safeAreaInsets.bottom
                }
                itemSize = CGSize(width: width, height: (scheduleView.bounds.height - verticalInset - verticalPadding) / CGFloat(tickCount))
            } else {
                itemSize = CGSize(width: width, height: width * gridRadio)
            }
            gridFrame = CGRect(origin: CGPoint(x: itemSize.width + padding + 8, y: (showWeekdayLabels ? ScheduleContentView.LABEL_HEIGHT + 8 : 0) + padding),
                    size: CGSize(width: itemSize.width * 7, height: itemSize.height * CGFloat(tickCount)))
            dragLength = itemSize.height * CGFloat(minutesPerStep) / CGFloat(minutesPerTick)
            heightConstraint.constant = verticalPadding + gridFrame.height
            gridLayer.frame = bounds
            gridLayer.itemSize = itemSize
            gridLayer.gridFrame = gridFrame
            controlView.itemSize = itemSize
            controlView.gridFrame = gridFrame
            controlView.dragLength = dragLength
            layoutLabels()
        }
        super.layoutSubviews()
    }

    override func draw(_ rect: CGRect) {
        var iconPoints = [(icon: UIImage, point: CGPoint)]()
        for model in models {
            let rect = rectInGrid(dayOfWeek: model.dayOfWeek, timeInterval: model.timeInterval)
            model.color.setFill()
            UIBezierPath(rect: rect).fill()
            if let icon = model.icon {
                let x: CGFloat
                let y: CGFloat
                switch model.iconContentMode {
                case .top:
                    x = rect.midX - icon.size.width / 2
                    y = rect.minY - icon.size.height / 2
                case .bottom:
                    x = rect.midX - icon.size.width / 2
                    y = rect.maxY - icon.size.height / 2
                case .left:
                    x = rect.minX - icon.size.width / 2
                    y = rect.midY - icon.size.height / 2
                case .right:
                    x = rect.maxX - icon.size.width / 2
                    y = rect.midY - icon.size.height / 2
                case .topLeft:
                    x = rect.minX - icon.size.width / 2
                    y = rect.minY - icon.size.height / 2
                case .topRight:
                    x = rect.maxX - icon.size.width / 2
                    y = rect.minY - icon.size.height / 2
                case .bottomLeft:
                    x = rect.minX - icon.size.width / 2
                    y = rect.maxY - icon.size.height / 2
                case .bottomRight:
                    x = rect.maxX - icon.size.width / 2
                    y = rect.maxY - icon.size.height / 2
                default:
                    x = rect.midX - icon.size.width / 2
                    y = rect.midY - icon.size.height / 2
                }
                iconPoints.append((icon, CGPoint(x: x, y: y)))
            }
        }
        for iconPoint in iconPoints {
            iconPoint.icon.draw(at: iconPoint.point)
        }
    }

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer.numberOfTouches > 1 else {
            return false
        }
        if gridRadio == 0 {
            let gridRadio = itemSize.height / itemSize.width
            gridRadioBounds = (gridRadio * zoomScale.minimum, gridRadio * zoomScale.maximum)
        }
        pinchInitialState = (
                itemSize.height,
                abs(gestureRecognizer.location(ofTouch: 0, in: self).y - gestureRecognizer.location(ofTouch: 1, in: self).y)
        )
        return true
    }

    private func layoutLabels() {
        for (index, label) in dayLabels.enumerated() {
            label.frame = CGRect(x: gridFrame.minX + CGFloat(index) * itemSize.width,
                    y: padding,
                    width: itemSize.width,
                    height: ScheduleContentView.LABEL_HEIGHT)
        }
        let yOffset = gridFrame.minY - ScheduleContentView.LABEL_HEIGHT / 2
        for (index, label) in timeLabels.enumerated() {
            label.frame = CGRect(x: padding,
                    y: yOffset + CGFloat(index) * itemSize.height,
                    width: itemSize.width,
                    height: ScheduleContentView.LABEL_HEIGHT)
        }
    }

    @objc private func tap(gestureRecognizer: UIGestureRecognizer) {
        let point = gestureRecognizer.location(in: self)
        guard gridFrame.contains(point),
              let dayOfWeek = ScheduleModel.DayOfWeek(rawValue: Int((point.x - gridFrame.minX) / itemSize.width)) else {
            return
        }
        var previous: ScheduleModel?
        let current: ScheduleModel
        var next: ScheduleModel?
        if let index = models.firstIndex(where: { rectInGrid(dayOfWeek: $0.dayOfWeek, timeInterval: $0.timeInterval).contains(point) }) {
            current = models[index]
            guard current.isEnabled else {
                return
            }
            models.remove(at: index)
            for model in models {
                if model.dayOfWeek == dayOfWeek {
                    let rect = rectInGrid(dayOfWeek: model.dayOfWeek, timeInterval: model.timeInterval)
                    if point.y > rect.maxY {
                        if dragOverflow {
                            if !current.isMergeable(model: model) {
                                previous = model
                            }
                        } else {
                            previous = model
                        }
                    } else {
                        if dragOverflow {
                            if !current.isMergeable(model: model) {
                                next = model
                                break
                            }
                        } else {
                            next = model
                            break
                        }
                    }
                }
            }
        } else {
            for model in models {
                if model.dayOfWeek == dayOfWeek {
                    let rect = rectInGrid(dayOfWeek: model.dayOfWeek, timeInterval: model.timeInterval)
                    if point.y > rect.maxY {
                        if dragOverflow {
                            if !model.isEnabled || model.color != barColor || model.selectedColor != barSelectedColor {
                                previous = model
                            }
                        } else {
                            previous = model
                        }
                    } else {
                        if dragOverflow {
                            if !model.isEnabled || model.color != barColor || model.selectedColor != barSelectedColor {
                                next = model
                                break
                            }
                        } else {
                            next = model
                            break
                        }
                    }
                }
            }
            let startStepIndex = Int((point.y - gridFrame.minY) / (gridFrame.height / (1440 / CGFloat(createMinutes))))
            var startMinutes = startStepIndex * createMinutes
            var endMinutes = (startStepIndex + 1) * createMinutes
            let startBound = max(previous?.timeInterval.end.valueInMinutes ?? 0, startMinutes)
            let endBound = min(next?.timeInterval.start.valueInMinutes ?? 1440, endMinutes)
//            let startBound = previous?.timeInterval.end.valueInMinutes ?? 0
//            let endBound = next?.timeInterval.start.valueInMinutes ?? 1440
            let startDifference = startMinutes - startBound
            if startDifference < 0 {
                startMinutes = startBound
                endMinutes = min(endMinutes - startDifference, endBound)
            } else {
                let endDifference = endMinutes - endBound
                if endDifference > 0 {
                    endMinutes = endBound
                    startMinutes = max(startMinutes - endDifference, startBound)
                }
            }
            current = ScheduleModel(
                    isEnabled: true,
                    color: barColor,
                    selectedColor: barSelectedColor,
                    dayOfWeek: dayOfWeek,
                    timeInterval: (ScheduleModel.ScheduleTime(valueInMinutes: startMinutes),
                            ScheduleModel.ScheduleTime(valueInMinutes: endMinutes)))
        }
        tapGesture.isEnabled = false
        longPressGesture.isEnabled = false
        controlView.models = (previous, current, next)
        delegate?.scheduleContentWillBeginEditing(model: current)
    }

    @objc private func longPress(gestureRecognizer: UIGestureRecognizer) {
        guard gestureRecognizer.state == .began else {
            return
        }
        let point = gestureRecognizer.location(in: self)
        guard gridFrame.contains(point),
              let dayOfWeek = ScheduleModel.DayOfWeek(rawValue: Int((point.x - gridFrame.minX) / itemSize.width)) else {
            return
        }
        let startStepIndex = Int((point.y - gridFrame.minY) / (gridFrame.height / (1440 / CGFloat(createMinutes))))
        let startMinutes = startStepIndex * createMinutes
        let endMinutes = (startStepIndex + 1) * createMinutes
        delegate?.scheduleContentDidLongPress(dayOfWeek: dayOfWeek,
                timeInterval: (ScheduleModel.ScheduleTime(valueInMinutes: startMinutes),
                        ScheduleModel.ScheduleTime(valueInMinutes: endMinutes)))
    }

    @objc private func pinch(gestureRecognizer: UIGestureRecognizer) {
        guard gestureRecognizer.numberOfTouches > 1 else {
            return
        }
        let height = abs(gestureRecognizer.location(ofTouch: 0, in: self).y - gestureRecognizer.location(ofTouch: 1, in: self).y)
        let heightDifference = height - pinchInitialState.touchPositionDifference
        let ratio = (pinchInitialState.itemHeight + heightDifference) / itemSize.width
        if heightDifference > 0 {
            if ratio > gridRadioBounds.maximum {
                gridRadio = gridRadioBounds.maximum
                pinchInitialState = (itemSize.height, height)
            } else {
                gridRadio = ratio
            }
        } else if heightDifference < 0 {
            if ratio < gridRadioBounds.minimum {
                gridRadio = gridRadioBounds.minimum
                pinchInitialState = (itemSize.height, height)
            } else {
                gridRadio = ratio
            }
        }
        guard let scheduleView = superview as? ScheduleView else {
            return
        }
        scheduleView.gridRadio = gridRadio
        let verticalPadding = (showWeekdayLabels ? ScheduleContentView.LABEL_HEIGHT + 8 : 0) + padding * 2
        let oldContentHeight = heightConstraint.constant
        let newContentHeight = verticalPadding + itemSize.width * gridRadio * CGFloat(tickCount)
        let location = gestureRecognizer.location(in: self)
        let anchor = location.y - scheduleView.contentOffset.y
        heightConstraint.constant = newContentHeight
        scheduleView.contentOffset.y = location.y * newContentHeight / oldContentHeight - anchor
    }

    func rectInGrid(dayOfWeek: ScheduleModel.DayOfWeek,
                    timeInterval: (start: ScheduleModel.ScheduleTime, end: ScheduleModel.ScheduleTime)) -> CGRect {
        return CGRect(
                origin: CGPoint(
                        x: gridFrame.minX + itemSize.width * CGFloat(dayOfWeek.rawValue),
                        y: gridFrame.minY + (CGFloat(timeInterval.start.hour) * 60 + CGFloat(timeInterval.start.minute)) / CGFloat(minutesPerTick) * itemSize.height),
                size: CGSize(
                        width: itemSize.width,
                        height: (CGFloat(timeInterval.end.hour - timeInterval.start.hour) * 60 + CGFloat(timeInterval.end.minute - timeInterval.start.minute)) / CGFloat(minutesPerTick) * itemSize.height))
    }

    func scheduleControlDidTapEdit(model: ScheduleModel) {
        delegate?.scheduleContentDidTapEdit(model: model)
    }

    func scheduleControlDidEndEditing(model: ScheduleModel) {
        models.append(model)
        tapGesture.isEnabled = true
        longPressGesture.isEnabled = true
        delegate?.scheduleContentDidChange(models: models)
    }

    func scheduleControlDidRemove(model: ScheduleModel) {
        tapGesture.isEnabled = true
        longPressGesture.isEnabled = true
        delegate?.scheduleContentDidChange(models: models)
    }

    func endEditing(save: Bool) {
        if let model = controlView.endEditing(), save {
            models.append(model)
        }
        tapGesture.isEnabled = true
        longPressGesture.isEnabled = true
    }
}

protocol ScheduleContentViewDelegate: AnyObject {
    func scheduleContentWillBeginEditing(model: ScheduleModel)

    func scheduleContentDidTapEdit(model: ScheduleModel)

    func scheduleContentDidLongPress(dayOfWeek: ScheduleModel.DayOfWeek,
                                     timeInterval: (start: ScheduleModel.ScheduleTime, end: ScheduleModel.ScheduleTime))

    func scheduleContentDidChange(models: [ScheduleModel])
}
