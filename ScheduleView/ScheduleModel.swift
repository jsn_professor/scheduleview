//
//  ScheduleModel.swift
//  ScheduleView
//
//  Created by Professor on 2021/2/5.
//

import UIKit

public struct ScheduleModel: Comparable {
    public let isEnabled: Bool
    public let color: UIColor
    public let selectedColor: UIColor
    public let dayOfWeek: DayOfWeek
    public var timeInterval: (start: ScheduleTime, end: ScheduleTime) {
        didSet {
            guard timeInterval.start < timeInterval.end else {
                fatalError("Start should be less than end")
            }
        }
    }
    public var icon: UIImage?
    public var iconContentMode: UIView.ContentMode?

    public init(isEnabled: Bool, color: UIColor, selectedColor: UIColor, dayOfWeek: DayOfWeek, timeInterval: (start: ScheduleTime, end: ScheduleTime), icon: UIImage? = nil, iconContentMode: UIView.ContentMode? = nil) {
        guard timeInterval.start < timeInterval.end else {
            fatalError("Start should be less than end")
        }
        self.isEnabled = isEnabled
        self.color = color
        self.selectedColor = selectedColor
        self.dayOfWeek = dayOfWeek
        self.timeInterval = timeInterval
        self.icon = icon
        self.iconContentMode = iconContentMode
    }

    func isMergeable(model: ScheduleModel) -> Bool {
        return isEnabled == model.isEnabled && color == model.color && selectedColor == model.selectedColor
    }

    public static func <(lhs: Self, rhs: Self) -> Bool {
        return lhs.dayOfWeek == rhs.dayOfWeek ? lhs.timeInterval.start < rhs.timeInterval.start : lhs.dayOfWeek < rhs.dayOfWeek
    }

    public static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.dayOfWeek == rhs.dayOfWeek && lhs.timeInterval.start == rhs.timeInterval.start && lhs.timeInterval.end == rhs.timeInterval.end
    }

    public enum DayOfWeek: Int, CaseIterable {
        case sunday, monday, tuesday, wednesday, thursday, friday, saturday

        public static func <(lhs: Self, rhs: Self) -> Bool {
            return lhs.rawValue < rhs.rawValue
        }

        public var localized: String {
            return DateFormatter().weekdaySymbols[rawValue]
        }
    }

    public struct ScheduleTime: Comparable {
        public var hour: Int {
            get {
                return Int(valueInMinutes / 60)
            }
            set {
                valueInMinutes = newValue * 60 + minute
            }
        }
        public var minute: Int {
            get {
                return Int(valueInMinutes % 60)
            }
            set {
                valueInMinutes = hour * 60 + newValue
            }
        }
        var valueInMinutes: Int

        init(valueInMinutes: Int) {
            self.valueInMinutes = valueInMinutes
        }

        public init(hour: Int, minute: Int) {
            if hour < 0 || hour > 24 {
                fatalError("Hour should be 0~24")
            }
            if minute < 0 || minute > 60 {
                fatalError("Minute should be 0~60")
            }
            valueInMinutes = hour * 60 + minute
        }

        public static func <(lhs: Self, rhs: Self) -> Bool {
            return lhs.valueInMinutes < rhs.valueInMinutes
        }

        public static func ==(lhs: Self, rhs: Self) -> Bool {
            return lhs.valueInMinutes == rhs.valueInMinutes
        }
    }
}
