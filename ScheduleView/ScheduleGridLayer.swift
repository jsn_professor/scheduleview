//
//  ScheduleGridLayer.swift
//  ScheduleView
//
//  Created by Professor on 2021/2/26.
//

import UIKit

class ScheduleGridLayer: CALayer {
//    override class func fadeDuration() -> CFTimeInterval {
//        return 0
//    }

    var verticalDashLines: Bool {
        didSet {
            guard oldValue != verticalDashLines else {
                return
            }
            setNeedsDisplay()
        }
    }
    var horizontalDashLines: Bool {
        didSet {
            guard oldValue != horizontalDashLines else {
                return
            }
            setNeedsDisplay()
        }
    }
    var gridColor: UIColor {
        didSet {
            guard oldValue != gridColor else {
                return
            }
            setNeedsDisplay()
        }
    }
    var minutesPerTick: Int {
        didSet {
            guard oldValue != minutesPerTick else {
                return
            }
            setNeedsDisplay()
        }
    }
    var itemSize = CGSize.zero {
        didSet {
            guard oldValue != itemSize else {
                return
            }
            setNeedsDisplay()
        }
    }
    var gridFrame = CGRect.zero {
        didSet {
            guard oldValue != gridFrame else {
                return
            }
            setNeedsDisplay()
        }
    }

    override func action(forKey event: String) -> CAAction? {
        return nil
    }

    init(verticalDashLines: Bool,
         horizontalDashLines: Bool,
         gridColor: UIColor,
         minutesPerTick: Int) {
        self.verticalDashLines = verticalDashLines
        self.horizontalDashLines = horizontalDashLines
        self.minutesPerTick = minutesPerTick
        self.gridColor = gridColor
        super.init()
        contentsScale = UIScreen.main.scale
    }

    override init(layer: Any) {
        let layer = layer as! ScheduleGridLayer
        self.verticalDashLines = layer.verticalDashLines
        self.horizontalDashLines = layer.horizontalDashLines
        self.minutesPerTick = layer.minutesPerTick
        self.gridColor = layer.gridColor
        super.init(layer: layer)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(in ctx: CGContext) {
        ctx.setStrokeColor(gridColor.cgColor)
        let dash: [CGFloat] = [2, 2]
        ctx.addRect(gridFrame)
        ctx.strokePath()
        if verticalDashLines {
            ctx.setLineDash(phase: 0, lengths: dash)
        }
        for index in 1...6 {
            let x = gridFrame.minX + CGFloat(index) * itemSize.width
            ctx.move(to: CGPoint(x: x, y: gridFrame.minY))
            ctx.addLine(to: CGPoint(x: x, y: gridFrame.maxY))
        }
        ctx.strokePath()
        if horizontalDashLines {
            ctx.setLineDash(phase: 0, lengths: dash)
        }
        for index in 1...24 * 60 / minutesPerTick - 1 {
            let y = gridFrame.minY + CGFloat(index) * itemSize.height
            ctx.move(to: CGPoint(x: gridFrame.minX, y: y))
            ctx.addLine(to: CGPoint(x: gridFrame.maxX, y: y))
        }
        ctx.strokePath()
    }
}
