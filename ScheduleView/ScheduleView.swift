//
//  ScheduleView.swift
//  iOSTemplate
//
//  Created by Professor on 26/10/2017.
//  Copyright © 2017 Agrowood. All rights reserved.
//

import UIKit

public class ScheduleView: UIScrollView, ScheduleContentViewDelegate {
    public override class var requiresConstraintBasedLayout: Bool {
        return true
    }
    @IBInspectable public var alwaysShowDragLabel: Bool = false {
        didSet {
            contentView?.controlView.alwaysShowDragLabel = alwaysShowDragLabel
        }
    }
    @IBInspectable public var showWeekdayLabels: Bool = true {
        didSet {
            contentView?.showWeekdayLabels = showWeekdayLabels
        }
    }
    @IBInspectable public var dragOverflow: Bool = false {
        didSet {
            contentView?.dragOverflow = dragOverflow
        }
    }
    @IBInspectable public var verticalDashLines: Bool = false {
        didSet {
            contentView?.gridLayer.verticalDashLines = showWeekdayLabels
        }
    }
    @IBInspectable public var horizontalDashLines: Bool = true {
        didSet {
            contentView?.gridLayer.horizontalDashLines = showWeekdayLabels
        }
    }
    @IBInspectable public var dragColor: UIColor? = nil {
        didSet {
            contentView?.controlView.dragColor = dragColor
        }
    }
    @IBInspectable public var barColor: UIColor = UIColor.orange.withAlphaComponent(0.5) {
        didSet {
            contentView?.barColor = barColor
        }
    }
    @IBInspectable public var barSelectedColor: UIColor = .orange {
        didSet {
            contentView?.barSelectedColor = barSelectedColor
        }
    }
    @IBInspectable public var labelColor: UIColor = .darkText {
        didSet {
            contentView?.labelColor = labelColor
        }
    }
    @IBInspectable public var gridColor: UIColor = .darkText {
        didSet {
            contentView?.gridLayer.gridColor = gridColor
        }
    }
    @IBInspectable public var padding: CGFloat = 8 {
        didSet {
            contentView?.padding = padding
            contentView?.controlView.padding = padding
        }
    }
    @IBInspectable public var gridRadio: CGFloat = 1 {
        didSet {
            contentView?.gridRadio = gridRadio
        }
    }
    @IBInspectable var createMinutes: Int = 60 {
        didSet {
            createMinutes = roundMinutes(minutes: createMinutes)
            contentView?.createMinutes = createMinutes
        }
    }
    @IBInspectable var minutesPerTick: Int = 60 {
        didSet {
            minutesPerTick = roundMinutes(minutes: minutesPerTick)
            contentView?.minutesPerTick = minutesPerTick
            contentView?.gridLayer.minutesPerTick = minutesPerTick
            contentView?.controlView.minutesPerTick = minutesPerTick
        }
    }
    @IBInspectable var minutesPerStep: Int = 60 {
        didSet {
            minutesPerStep = roundMinutes(minutes: minutesPerStep)
            contentView?.minutesPerStep = minutesPerStep
        }
    }
    public override var minimumZoomScale: CGFloat {
        didSet {
            contentView.zoomScale = (minimumZoomScale, maximumZoomScale)
        }
    }

    public override var maximumZoomScale: CGFloat {
        didSet {
            contentView.zoomScale = (minimumZoomScale, maximumZoomScale)
        }
    }
    public var topButtonImage: UIImage? {
        set {
            contentView.controlView.topButtonImage = newValue
        }
        get {
            return contentView.controlView.topButtonImage
        }
    }
    public var editButtonImage: UIImage? {
        set {
            contentView.controlView.editButtonImage = newValue
        }
        get {
            return contentView.controlView.editButtonImage
        }
    }
    public var bottomButtonImage: UIImage? {
        set {
            contentView.controlView.bottomButtonImage = newValue
        }
        get {
            return contentView.controlView.bottomButtonImage
        }
    }
    public var topButtonContentMode: UIView.ContentMode {
        set {
            contentView.controlView.topButtonContentMode = newValue
        }
        get {
            return contentView.controlView.topButtonContentMode
        }
    }
    public var bottomButtonContentMode: UIView.ContentMode {
        set {
            contentView.controlView.bottomButtonContentMode = newValue
        }
        get {
            return contentView.controlView.bottomButtonContentMode
        }
    }
    private var contentView: ScheduleContentView!
    public var models: [ScheduleModel] {
        get {
            return contentView.models
        }
        set(newValue) {
            contentView.models = newValue
        }
    }
    public weak var dataSource: ScheduleViewDataSource?

    public override func awakeFromNib() {
        translatesAutoresizingMaskIntoConstraints = false
        canCancelContentTouches = false
        contentView = ScheduleContentView(
                frame: bounds,
                alwaysShowDragLabel: alwaysShowDragLabel,
                showWeekdayLabels: showWeekdayLabels,
                dragOverflow: dragOverflow,
                verticalDashLines: verticalDashLines,
                horizontalDashLines: horizontalDashLines,
                createMinutes: createMinutes,
                minutesPerTick: minutesPerTick,
                minutesPerStep: minutesPerStep,
                padding: padding,
                gridRadio: gridRadio,
                zoomScale: (minimumZoomScale, maximumZoomScale),
                dragColor: dragColor,
                barColor: barColor,
                barSelectedColor: barSelectedColor,
                labelColor: labelColor,
                gridColor: gridColor)
        contentView.delegate = self
        addSubview(contentView)
        addConstraints([contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
                        contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
                        contentView.topAnchor.constraint(equalTo: topAnchor),
                        contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
                        contentView.widthAnchor.constraint(equalTo: widthAnchor)])
    }

    func scheduleContentWillBeginEditing(model: ScheduleModel) {
        dataSource?.scheduleWillBeginEditing(model: model)
    }

    func scheduleContentDidTapEdit(model: ScheduleModel) {
        dataSource?.scheduleDidTapEdit(model: model)
    }

    func scheduleContentDidLongPress(dayOfWeek: ScheduleModel.DayOfWeek,
                                     timeInterval: (start: ScheduleModel.ScheduleTime, end: ScheduleModel.ScheduleTime)) {
        dataSource?.scheduleDidLongPress(dayOfWeek: dayOfWeek, timeInterval: timeInterval)
    }

    func scheduleContentDidChange(models: [ScheduleModel]) {
        dataSource?.scheduleDidChange(models: models)
    }

    public func endEditing(save: Bool) {
        contentView.endEditing(save: save)
    }

    public func rect(dayOfWeek: ScheduleModel.DayOfWeek, timeInterval: (start: ScheduleModel.ScheduleTime, end: ScheduleModel.ScheduleTime)) -> CGRect {
        return convert(contentView.rectInGrid(dayOfWeek: dayOfWeek, timeInterval: timeInterval), from: contentView)
    }
}

private func roundMinutes(minutes: Int) -> Int {
    guard minutes > 0 else {
        return 1
    }
    guard minutes < 1440 else {
        return 1440
    }
    var delta = 0
    while minutes - delta > 0 || minutes + delta <= 1440 {
        if 1440 % (minutes + delta) == 0 {
            return minutes + delta
        } else if 1440 % (minutes - delta) == 0 {
            return minutes - delta
        }
        delta += 1
    }
    return minutes
}

public protocol ScheduleViewDataSource: AnyObject {
    func scheduleWillBeginEditing(model: ScheduleModel)

    func scheduleDidTapEdit(model: ScheduleModel)

    func scheduleDidLongPress(dayOfWeek: ScheduleModel.DayOfWeek,
                              timeInterval: (start: ScheduleModel.ScheduleTime, end: ScheduleModel.ScheduleTime))

    func scheduleDidChange(models: [ScheduleModel])
}
